<?php

require_once(__DIR__.'/../controller/IndexController.php');

$cnt = new IndexController();
$carlist = $cnt->randomListAction();

?><html>
    <head>
        <title>MVC Car Sample</title>
    </head>
    <body>
        <div id="wrapper">
          <div id="formulari">
            <form action="enviar.php" method="post">
            <div>
              <select name="Marca">
                <option value="BMW">BMW</option>
                <option value="Mercedes">Mercedes</option>
                <option value="VW">VW</option>
                <option value="Renault">Renault</option>
              </select>
            </div>
            <div>
              <label for="Modelo">Modelo:</label>
              <input type="text" id="Modelo" />
            </div>
            <div>
              <label for="Combustible">Combustible:</label>
              <input type="text" id="Combustible" />
            </div>
            <div>
              <label for="Color">Color:</label>
              <input type="text" id="Color" />
            </div>
            <div class="button">
              <button type="submit">Enviar</button>
            </div>
          </form>
          </div>

            <h1>MVC Car Sample</h1>
            <table>
                <tr><th>Marca</th><th>Model</th><th>Combustible</th><th>Color</th></tr>
                <?php foreach($carlist as $car){ ?>
                <tr>
                    <td><?=$car->getBrand()?></td>
                    <td><?=$car->getModel()?></td>
                    <td><?=$car->getGas()?></td>
                    <td><?=$car->getColor()?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </body>
</html>
