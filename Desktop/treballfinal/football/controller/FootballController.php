<?php

require_once(__DIR__.'/../model/db/TeamDb.php');

class FootballController{


    public function getTeams(){
       $db = new TeamDb();
       $teams = $db->findAllTeams();
       return $teams;
    }

    public function saveTeam($team){
        $db = new TeamDb();
        $id = $db->insertTeam($team);
        return $id;
    }

}
