<?php

require_once(__DIR__.'/../model/db/FrasesDb.php');
require_once(__DIR__.'/../model/Frases.php');



class FrasesController{

  public function getFrase($fid){
        $db = new FrasesDb();
        return $db->getFrase($fid);
    }

    public function getFrasesById($idfrases){
        $db = new FrasesDb();
        return $db->getFrasesById($idfrases);
    }

    public function getFrasesByAutor($idautor){
        $db = new FrasesDb();
        return $db->listFrasesByAutor($idautor);
    }

    public function createFrases($f, $autorid){
        $db = new FrasesDb();
        return $db->insertFrases($f, $autorid);
    }

    public function updateFrases($frase, $idfrases){
        $db = new FrasesDb();
        return $db->updateFrases($frase, $idfrases);
    }

    public function deleteFrases($idfrases){
        $db = new FrasesDb();
        return $db->deleteFrases($idfrases);
    }


}
