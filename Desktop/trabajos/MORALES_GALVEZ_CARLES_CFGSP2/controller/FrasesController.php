<?php

require_once(__DIR__.'/../model/db/FrasesDb.php');


class SeasonsController{

    public function getFrasesById($idfrases){
        $db = new FrasesDb();
        return $db->getSeasonById($idfrases);
    }

    public function getFrasesByAutor($idautor){
        $db = new FrasesDb();
        return $db->listFrasesByAutor($idautor);
    }

    public function createFrases($f, $autorid){
        $db = new FrasesDb();
        return $db->insertFrases($f, $autorid);
    }

    public function updateFrases($frase, $autorid){
        $db = new FrasesDb();
        return $db->updateFrases($frase, $autorid);
    }

    public function deleteFrases($frasesid){
        $db = new SeasonDb();
        return $db->deleteFrases($frasesid);
    }

}
