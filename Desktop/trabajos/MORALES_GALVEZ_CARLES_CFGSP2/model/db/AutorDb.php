<<?php

require_once(__DIR__.'/../Autor.php');
require_once(__DIR__.'/../../inc/Constants.php');


class AutorDb{

    private $conn;

    public function getAutor($id){
        $this->openConnection();

        $sql = "SELECT * FROM autornfrases WHERE sid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $sid);
        $sid = $id;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $autor = new Autor($r['nom'], $r['cognom'], $r['nfrases'], $r['sid']);

        return $autor;
    }

    public function getAutor(){
        $this->openConnection();

        $sql = "SELECT * FROM autornfrases";
        $stm = $this->conn->prepare($sql);

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $serie = new Autor($r['nom'], $r['cognom'], $r['nfrases'], $r['sid']);
            array_push($ret, $autor);
        }
        return $ret;
    }

    public function insertAutor($n, $c){
        $this->openConnection();

        $sql = "INSERT INTO autor (nom, cognom) VALUES (?, ?)";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("sis", $sn, $sc);
        $sn = $n;
        $sc = $c;

        $stm->execute();


        return new Autor($n, $c, null);

    }

    public function updateSerie($n, $c, $i){
        $this->openConnection();

        $sql = "UPDATE autor SET nom = ?, cognom = ? WHERE sid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("sisi", $sn, $sc, $si);
        $sn = $n;
        $sc = $c;
        $si = $i;

        $stm->execute();

        return $this->getAutor($i);
    }

    public function deleteAutor($id){
        $autor = $this->getAutor($id);
        $this->openConnection();

        $sql = "DELETE FROM autor WHERE sid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $sid);
        $sid = $id;

        $stm->execute();

        return $autor;
    }

    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}
