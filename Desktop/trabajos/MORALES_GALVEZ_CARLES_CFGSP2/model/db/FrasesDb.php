<?php

require_once(__DIR__.'/AutorDb.php');
require_once(__DIR__.'/../Frases.php');
require_once(__DIR__.'/../Autor.php');
require_once(__DIR__.'/../../inc/Constants.php');

class FrasesDb{

    private $conn;

    public function getFrasesById($idfrases){
        $this->openConnection();

        $sql = "SELECT * FROM frases WHERE idfrases = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idf);
        $idf = $idfrases;

        $stm->execute();
        $result = $stm->get_result();

        $row = $result->fetch_assoc();
        $frases = new Frases($row['frase'],$row['autor'], $row['idfrases'],null);

        $dbautor = new AutorDb();
        $autorinstance = $dbautor->getAutor($row['idautor']);
        $frases->setAutor($autorinstance);

        return $frases;
    }

    public function listFrasesByAutor($idautor){
        $dbautor = new AutorDb();
        $autorinstance = $dbautor->getAutor($idautor);

        $this->openConnection();

        $sql = "SELECT * FROM frases WHERE idautor = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $sid);
        $sid = $idautor;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($row = $result->fetch_assoc()){
            $frases = new Frases($row['frase'],$row['autor'],$row['idfrases'],$autorinstance);
            array_push($ret, $frases);
        }
        return $ret;
    }

    public function insertFrases($f, $a, $autorid){
        $this->openConnection();

        $sql = "INSERT INTO frases (idautor, frase, autor) VALUES (?, ?, ?)";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("iiis", $sida, $sn, $sf, $sa);
        $sida = $autorid;
        $sn = $n;
        $sf = $nf;
        $sa = $sa;

        $stm->execute();


        return null;

    }

    public function updateFrases($frase, $autor, $frasesid){
        $this->openConnection();

        $sql = "UPDATE season SET year = ?, nchapters = ?, season = ? WHERE idseason = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("iisi", $sf, $sa, $ids);
        $sn = $nom;
        $sa = $autor;
        $idf = $frasesid;

        $stm->execute();

        return $this->getFrasesById($frasesid);
    }

    public function deleteFrases($frasesid){
        $frases = $this->getFrasesById($frasesid);
        $this->openConnection();

        $sql = "DELETE FROM frases WHERE idfrases  = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $sid);
        $sid = $frasesid;

        $stm->execute();

        return $frases;
    }


    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}
