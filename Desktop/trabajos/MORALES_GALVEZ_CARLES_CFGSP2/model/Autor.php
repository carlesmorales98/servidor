<?php

class Autor{

    private $_sid;
    private $_nom;
    private $_cognom;
    private $_nfrases;

    public function __construct($n, $c, $nf=null, $id = null){
        $this->setNom($n);
        $this->setCognom($c);
        $this->setNfrases($nf);
        $this->setSid($id);
    }

    public function getSid(){
        return $this->_sid;
    }

    public function getNom(){
        return $this->_nom;
    }

    public function getCognom(){
        return $this->_cognom;
    }

    public function getNfrases(){
        return $this->_nfrases;
    }

    public function setSid($_sid){
        $this->_sid = $_sid;
    }

    public function setNom($_nom){
        $this->_nom = $_nom;
    }

    public function setCognom($_cognom){
        $this->_cognom = $_cognom;
    }

    public function setNfrases($_nfrases){
        $this->_nfrases = $_nfrases;
    }

}
