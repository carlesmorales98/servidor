<?php

require_once(__DIR__.'/../Currency.php');

class CurrencyDb{

  private $conn;

public function getCurrencies(){
  $this->openConnection();

  $sql = "SELECT * FROM currency";
  $stm->conn->prepare($sql);

  $stm->execute();
  $result = $stm->get_result();

  $ret = array();

  while($r=result->fetch_assoc()){
    $curr = new Currency($r['symbol'],
                         $r['name'],
                         $r['eurval'],
                         $r['type']);

    array_push($ret, $curr);
  }

  return $ret;

}

public function createCurrency($cn, $cs, $cev, $ct ){
  $this->openConnection();

  $sql = 'INSERT INTO currency (name, symbol, type, eurval) VALUES (?, ?, ?, ?)';
  $stm = $this->conn->prepare($sql);

  $stm->bind_param("ssss", $n, $s, $t, $ev);
  $n = $cn;
  $s= $cs;
  $t = $ct;
  $ev= $cev;

  $stm->execute();
  $result = $stm->get_result();

  return new Currency($cn, $cs, $cev, $ct);
}


  private function openConnection(){
    if($this->conn= null){
      $this->conn = mysql_connect("127.0.0.1",
      "money",
      "Money123",
      "money");
    }
  }

}
