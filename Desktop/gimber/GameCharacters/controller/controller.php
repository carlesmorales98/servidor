<?php
require_once(__DIR__.'/../model/db/GameCDb.php');

class GameCController{

    /**
     * Create a character given all the attributes of the class
     *
     * @param String $name
     * @param String $specie
     * @param Integer $attack
     * @param Integer $defense
     * @return boolean
     */
    public function createCharacter($name, $specie,
            $attack = null, $defense = null){

        //Create GameC instance
        $gc = new GameC($name, $specie);
        $gc->setDefenseP($defense);
        $gc->setAttackP($attack);

        //Instantiate GameCDb
        $db = new GameCDb();

        //Create DB registry and return
        return $db->createGameC($gc);
    }

    public function listCharacters(){
        $db = new GameCDb();
        return $db->listGameC();
    }

    public function getCharacter(){
        $db = new GameCDb();
        return $db->getGameC();
    }

    public function modifyCharacter(){

    }

    public function removeCharacter(){

    }

}
