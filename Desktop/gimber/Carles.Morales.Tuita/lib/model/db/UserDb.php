<?php

class UserDb{

    public function getUserByUsername($un){
        $collection = (new MongoDB\Client)->tuita->user;
        $object = $collection->findOne(['username'=>$un]);

        return new User($object['username']);
    }

    public function insertUser($usr){
        $collection = (new MongoDB\Client)->tuita->user;

        $insertOneResult = $collection->insertOne($usr->toArray());
        $userIns = $this->getUserByUsername($usr->getUsername());

        return $userIns;
    }

    public function removeUser($username){
        $retusr = $this->getUserByUsername($username);

        $collection = (new MongoDB\Client)->tuita->user;
        $collection->deleteOne(['username'=>$username]);


        return $retusr;
    }

    public function addTuit2User($tuit){
        $tins = $tuit->toArray();
        $uname = $tins['usr'];
        unset($tins['usr']);

        $collection = (new MongoDB\Client)->tuita->user;
        $collection->updateOne(["username"=>$tuit->getUsr()],
                ['$push' => ['owntuits' => $tins]]);

        return $this->getUserByUsername($uname);
    }

    public function favTuit($uname, $tuid){
        $collection = (new MongoDB\Client)->tuita->user;
        $collection->updateOne(
            ["username"=>$uname,"owntuits.tuid"=>$tuid],
            ['$inc' => ['owntuits.$.fav' => 1]]);

        return $this->getUserByUsername($uname);
    }

}
